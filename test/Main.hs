module Main where

import Test.Hspec.Runner (
    hspec,
 )

import Spec (
    spec,
 )
import Test.Hspec (parallel)
import Test.Hspec.QuickCheck (modifyMaxShrinks)

main :: IO ()
main = hspec . parallel $ modifyMaxShrinks (const 0) $ spec
